$(document).ready(function () {

	Blocks_Height();
	Focus_and_Blur();


	$(window).resize(function () {
		Blocks_Height();
		Blocks_Media();
	});

	Blocks_Media();



	Top_Menu();



////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//Media
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	function Blocks_Media() {
		if ($(window).width() < 1700) {
			$('.Page-1 br, .Page-2 br, .Page-3 br, .Page-4 br, .Page-5 br, .Page-6 br').remove();
		}

	}




///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////





////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//Blocks Height
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	function Blocks_Height() {


		var Height = $(window).height();
		if (Height > 679) {
			$('.Page-1, .Page-2,.Page-3,.Page-4,.Page-5,.Page-6, .Page-7').css('height', Height);
		} else {
			$('.Page-1, .Page-2,.Page-3,.Page-4,.Page-5,.Page-6, .Page-7').css('height', '950');
		}
	}




///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////




////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//Top_Menu
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


	function Top_Menu() {
		var Animated = false;
		var Button = $('.Page-1 .Circle-Out');

		Button.click(Change_Page);

		$(window).on('scroll', function () {
			if(!Animated){
				var Index = 0;
				if ($(window).scrollTop() >= $('.Page-1').offset().top) {
					Index = 0;
				}
				if ($(window).scrollTop() >= $('.Page-2').offset().top - 20) {
					Index = 1;
				}
				if ($(window).scrollTop() >= $('.Page-3').offset().top - 20) {
					Index = 2;
				}
				if ($(window).scrollTop() >= $('.Page-4').offset().top - 20) {
					Index = 3;
				}
				if ($(window).scrollTop() >= $('.Page-5').offset().top - 20) {
					Index = 4;
				}
				if ($(window).scrollTop() >= $('.Page-6').offset().top - 20) {
					Index = 5;
				}
				if ($(window).scrollTop() >= $('.Page-7').offset().top - 20) {
					Index = 6;
				}

				$('.Page-1 .Circle-Second').css('opacity', '');
				$($('.Page-1 .Circle-Second')[Index]).css('opacity', '1');

				if (Index == 0) {
					$('.Page-1 .Switch span').css('color', '');
					$('.Page-1 .Circle-Out').css('border-color', '');
					$('.Page-1 .Circle-Second').css('background', '');
					$('.Page-1 .Switch-Hr').css('background-color', '');
				}

				if (Index == 1) {
					$('.Page-1 .Switch span').css('color', 'black');
					$('.Page-1 .Circle-Out').css('border-color', 'black');
					$('.Page-1 .Circle-Second').css('background', 'black');
					$('.Page-1 .Switch-Hr').css('background-color', 'black');
				}

				if (Index == 2) {
					$('.Page-1 .Switch span').css('color', '');
					$('.Page-1 .Circle-Out').css('border-color', '');
					$('.Page-1 .Circle-Second').css('background', '');
					$('.Page-1 .Switch-Hr').css('background-color', '');
				}

				if (Index == 3) {
					$('.Page-1 .Switch span').css('color', '');
					$('.Page-1 .Circle-Out').css('border-color', '');
					$('.Page-1 .Circle-Second').css('background', '');
					$('.Page-1 .Switch-Hr').css('background-color', '');
				}

				if (Index == 4) {
					$('.Page-1 .Switch span').css('color', '');
					$('.Page-1 .Circle-Out').css('border-color', '');
					$('.Page-1 .Circle-Second').css('background', '');
					$('.Page-1 .Switch-Hr').css('background-color', '');
				}

				if (Index == 5) {
					$('.Page-1 .Switch span').css('color', 'black');
					$('.Page-1 .Circle-Out').css('border-color', 'black');
					$('.Page-1 .Circle-Second').css('background', 'black');
					$('.Page-1 .Switch-Hr').css('background-color', 'black');
				}

				if (Index == 6) {
					$('.Page-1 .Switch span').css('color', '');
					$('.Page-1 .Circle-Out').css('border-color', '');
					$('.Page-1 .Circle-Second').css('background', '');
					$('.Page-1 .Switch-Hr').css('background-color', '');
				}
			}


		});

		$('body,html').on('mousewheel', function (e) {
			if (!Animated) {
				Animated = true;

				var Index = 0;
				if ($(window).scrollTop() >= $('.Page-1').offset().top) {
					Index = 0;
				}
				if ($(window).scrollTop() >= $('.Page-2').offset().top - 20) {
					Index = 1;
				}
				if ($(window).scrollTop() >= $('.Page-3').offset().top - 20) {
					Index = 2;
				}
				if ($(window).scrollTop() >= $('.Page-4').offset().top - 20) {
					Index = 3;
				}
				if ($(window).scrollTop() >= $('.Page-5').offset().top - 20) {
					Index = 4;
				}
				if ($(window).scrollTop() >= $('.Page-6').offset().top - 20) {
					Index = 5;
				}
				if ($(window).scrollTop() >= $('.Page-7').offset().top - 20) {
					Index = 6;
				}

				if (e.originalEvent.wheelDelta / 120 > 0) {
					if (Index > 0) {
						Index--;
					} else {
						Index = 6;
					}

					Change_Page(Index);
				}
				else {
					if (Index == 6) {
						Index = 0;
					} else {
						Index++;
					}

					Change_Page(Index);
				}

				setTimeout(function () {
					Animated = false;
				}, 1000);
			}
			return false;
		});

		function Change_Page(Index) {
			if (typeof Index != 'number') {
				Index = $(this).parents('.Switch').index() / 2;
			}


			$('.Page-1 .Circle-Second').css('opacity', '');
			$($('.Page-1 .Circle-Second')[Index]).css('opacity', '1');

			if (Index == 0) {
				$('body,html').animate({
					'scrollTop': 0
				}, 1000);
				$('.Page-1 .Switch span').css('color', '');
				$('.Page-1 .Circle-Out').css('border-color', '');
				$('.Page-1 .Circle-Second').css('background', '');
				$('.Page-1 .Switch-Hr').css('background-color', '');
			}

			if (Index == 1) {
				$('body,html').animate({
					'scrollTop': $('.Page-2').offset().top
				}, 1000);

				$('.Page-1 .Switch span').css('color', 'black');
				$('.Page-1 .Circle-Out').css('border-color', 'black');
				$('.Page-1 .Circle-Second').css('background', 'black');
				$('.Page-1 .Switch-Hr').css('background-color', 'black');
			}

			if (Index == 2) {
				$('body,html').animate({
					'scrollTop': $('.Page-3').offset().top
				}, 1000);
				$('.Page-1 .Switch span').css('color', '');
				$('.Page-1 .Circle-Out').css('border-color', '');
				$('.Page-1 .Circle-Second').css('background', '');
				$('.Page-1 .Switch-Hr').css('background-color', '');
			}

			if (Index == 3) {
				$('body,html').animate({
					'scrollTop': $('.Page-4').offset().top
				}, 1000);
				$('.Page-1 .Switch span').css('color', '');
				$('.Page-1 .Circle-Out').css('border-color', '');
				$('.Page-1 .Circle-Second').css('background', '');
				$('.Page-1 .Switch-Hr').css('background-color', '');
			}

			if (Index == 4) {
				$('body,html').animate({
					'scrollTop': $('.Page-5').offset().top
				}, 1000);
				$('.Page-1 .Switch span').css('color', '');
				$('.Page-1 .Circle-Out').css('border-color', '');
				$('.Page-1 .Circle-Second').css('background', '');
				$('.Page-1 .Switch-Hr').css('background-color', '');
			}

			if (Index == 5) {
				$('body,html').animate({
					'scrollTop': $('.Page-6').offset().top
				}, 1000);
				$('.Page-1 .Switch span').css('color', 'black');
				$('.Page-1 .Circle-Out').css('border-color', 'black');
				$('.Page-1 .Circle-Second').css('background', 'black');
				$('.Page-1 .Switch-Hr').css('background-color', 'black');
			}

			if (Index == 6) {
				$('body,html').animate({
					'scrollTop': $('.Page-7').offset().top
				}, 1000);
				$('.Page-1 .Switch span').css('color', '');
				$('.Page-1 .Circle-Out').css('border-color', '');
				$('.Page-1 .Circle-Second').css('background', '');
				$('.Page-1 .Switch-Hr').css('background-color', '');
			}
			return false;
		}
	}



///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////





////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Focus And Blur On Input
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	function Focus_and_Blur() {
		var Input_Field = 'input[type="text"],textarea';
		var Hold = $('.My-Placeholder-1, .My-Placeholder');
		$(document).on('focus', Input_Field, function () {
			$('+.My-Placeholder', this).css('display', 'none');
		});



		$(document).on('blur', Input_Field, function () {
			if (!$(this).val().length) {
				$('+label', this).css('display', 'block');
			}
		});
	}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////




	//$(".Page-Wrapper").onepage_scroll({
	//	afterMove: function (Index) {
	//		var Block_1 = $('.Page-1').height();
	//		var Block_2 = $('.Page-2').offset().top;
	//		var Block_3 = $('.Page-3').offset().top;
	//		var Block_4 = $('.Page-4').offset().top;
	//		var Block_5 = $('.Page-5').offset().top;
	//		var Block_6 = $('.Page-6').offset().top;
	//		var Block_7 = $('.Page-7').offset().top;
	//
	//		if ($('body').scrollTop() < Block_1 - 10) {
	//
	//			$('.onepage-pagination li').css('border-color', '');
	//			$(".onepage-pagination li a.active").removeClass('al');
	//			$('.Switch-Hr').removeClass('Bl');
	//		}
	//
	//
	//		if ($('body').scrollTop() >= Block_2 - 10) {
	//
	//			$('.onepage-pagination li').css('border-color', 'black');
	//			$(".onepage-pagination li a.active").addClass('al');
	//			$('.Switch-Hr').addClass('Bl');
	//
	//
	//
	//		}
	//
	//		if ($('body').scrollTop() >= Block_3 - 10) {
	//
	//			$('.onepage-pagination li').css('border-color', '');
	//			$(".onepage-pagination li a.active").removeClass('al');
	//			$('.Switch-Hr').removeClass('Bl');
	//
	//
	//		}
	//
	//		if ($('body').scrollTop() >= Block_4 - 10) {
	//
	//			$('.onepage-pagination li').css('border-color', '');
	//			$(".onepage-pagination li a.active").removeClass('al');
	//			$('.Switch-Hr').removeClass('Bl');
	//
	//
	//
	//		}
	//
	//		if ($('body').scrollTop() >= Block_5 - 10) {
	//
	//			$('.onepage-pagination li').css('border-color', '');
	//			$(".onepage-pagination li a.active").removeClass('al');
	//			$('.Switch-Hr').removeClass('Bl');
	//
	//
	//
	//		}
	//
	//		if ($('body').scrollTop() >= Block_6 - 10) {
	//
	//			$('.onepage-pagination li').css('border-color', 'black');
	//			$(".onepage-pagination li a.active").addClass('al');
	//			$('.Switch-Hr').addClass('Bl');
	//
	//
	//
	//		}
	//
	//		if ($('body').scrollTop() >= Block_7 - 10) {
	//
	//			$('.onepage-pagination li').css('border-color', '');
	//			$(".onepage-pagination li a.active").removeClass('al');
	//			$('.Switch-Hr').removeClass('Bl');
	//
	//
	//		}
	//
	//	}
	//});
	//
	//$('.onepage-pagination li').after("<div class='Switch-Hr'></div>");
	//
	//




});
 